﻿#include <iostream>
#include <ctime>

void FillArray(int *Array, const int ArraySize)
{
    for (int i = 0; i < ArraySize; ++i)
    {
        for (int j = 0; j < ArraySize; ++j)
        {
            *(Array + j + i * ArraySize) = i + j;
        }
    }
}

void PrintArray(int* Array, const int ArraySize)
{
    for (int i = 0; i < ArraySize; ++i)
    {
        for (int j = 0; j < ArraySize; ++j)
        {
            std::cout << *(Array + j + i * ArraySize) << "\t";
        }
        std::cout << std::endl;
    }
}

void PrintArrayRowSum(int* Array, const int ArraySize, const int Row)
{
    int Sum = 0;
    for (int i = 0; i < ArraySize; ++i)
    {
        Sum += *(Array + i + Row * ArraySize);
    }
    std::cout << "Sum at row# " << Row << ": " << Sum << std::endl;
}

int main()
{
    const int Size = 14;
    int Array[Size][Size];
    std::time_t Time = time(nullptr);
    std::tm Tm;
    localtime_s(&Tm, &Time);
    const int IndexSum = Tm.tm_mday % Size;

    FillArray(*Array, Size);
    PrintArray(*Array, Size);
    PrintArrayRowSum(*Array, Size, IndexSum);
}
